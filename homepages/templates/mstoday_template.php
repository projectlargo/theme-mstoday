<?php global $shown_ids; ?>

<div class="homepage-top-post">
	<?php echo $homepage_top_story; ?>
</div>
<div class="clearfix"></div>
<div class="homepage-top-rail">

	<div class="homepage-top-rail-left">
		<?php echo $homepage_second_story; ?>
	</div>

	<div class="homepage-top-rail-right">
		<?php echo $homepage_featured_projects_widget; ?>
	</div>
	
	<div class="clearfix"></div>

</div>

<div class="homepage-river">
	<?php echo $homepage_river; ?>
</div>